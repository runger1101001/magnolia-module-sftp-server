package info.magnolia.resourceloader.file;

import info.magnolia.resourceloader.Resource;

import java.nio.file.Path;

public class FileSystemResourceVisibilityHack {

	static public Path getRealPath(Resource r){
		return ((FileSystemResource)r).getRealPath();	
	}
	
}
