package info.magnolia.resourceloader.jcr;

import info.magnolia.resourceloader.Resource;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

public class JcrResourceVisibilityHack {

	public static Node getNode(Resource r){
		return ((JcrResource)r).getNode();
	}
	
	public static boolean isTextResource(Node resourceNode) throws RepositoryException {
        return resourceNode.hasProperty(JcrResourceOrigin.TEXT_PROPERTY);
    }

	public static boolean isBinaryResource(Node node) throws RepositoryException {
        return node.hasNode(JcrResourceOrigin.BINARY_NODE_NAME);
    }

}
