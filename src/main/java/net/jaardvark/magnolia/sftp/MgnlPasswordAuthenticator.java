package net.jaardvark.magnolia.sftp;

import info.magnolia.cms.security.SecuritySupport;
import info.magnolia.cms.security.SecuritySupportImpl;
import info.magnolia.cms.security.auth.callback.CredentialsCallbackHandler;
import info.magnolia.cms.security.auth.login.LoginResult;
import info.magnolia.context.MgnlContext;
import info.magnolia.context.MgnlContext.Op;
import info.magnolia.objectfactory.Components;

import org.apache.sshd.server.auth.password.PasswordAuthenticator;
import org.apache.sshd.server.session.ServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MgnlPasswordAuthenticator implements PasswordAuthenticator {

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(MgnlPasswordAuthenticator.class);
	
	protected SecuritySupport securitySupport;

	public MgnlPasswordAuthenticator(){
		this.securitySupport = Components.getComponent(SecuritySupport.class);
	}
	
	@Override
	public boolean authenticate(final String username, final String password, ServerSession session) {
		Boolean authCheck = MgnlContext.doInSystemContext(new Op<Boolean,RuntimeException>(){
			@Override
			public Boolean exec() {
				final CredentialsCallbackHandler credentialsCallbackHandler = new CredentialsCallbackHandler(username, password.toCharArray());
				LoginResult result = securitySupport.authenticate(credentialsCallbackHandler, SecuritySupportImpl.DEFAULT_JAAS_LOGIN_CHAIN);
				if (result.getStatus()==LoginResult.STATUS_SUCCEEDED)
					return true;
				return false;
			}
		});
		log.info("Password-authenticated SFTP user "+username+" with result "+authCheck);
		return authCheck.booleanValue();
	}

}
