package net.jaardvark.magnolia.sftp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;


public class SftpServerModule implements ModuleLifecycle {

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(SftpServerModule.class);


	public static final String MODULE_NAME = "sftp-server";
	
	
	protected int port = 2222;

	private MgnlSftpServer server;
	

	
	
	public void start(ModuleLifecycleContext arg0) {
		log.debug("SFTP Server Module starting.");
		server = new MgnlSftpServer();
		
		server.startServer(port, MODULE_NAME);
	}

	public void stop(ModuleLifecycleContext arg0) {
		log.debug("SFTP Server Module stopping.");
		server.stopServer();
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
