package net.jaardvark.magnolia.sftp;

import java.util.ArrayList;
import java.util.List;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.Task;

public class SftpServerModuleVersionHandler extends DefaultModuleVersionHandler {
	
	@Override
	protected List<Task> getExtraInstallTasks(InstallContext installContext) {
		List<Task> extraTasks = new ArrayList<Task>();
		// add any install-tasks here
		return extraTasks;
	}

}
