package net.jaardvark.magnolia.sftp;

import info.magnolia.cms.security.SecurityUtil;
import info.magnolia.context.MgnlContext;
import info.magnolia.context.MgnlContext.Op;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.jcr.util.SessionUtil;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sshd.common.keyprovider.AbstractKeyPairProvider;
import org.apache.sshd.common.util.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MgnlKeypairProvider extends AbstractKeyPairProvider {
	
	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(MgnlKeypairProvider.class);

	public static final String HOSTKEY_PRIV_PROPERTY_NAME = "hostKey";
	public static final String HOSTKEY_PUB_PROPERTY_NAME = "hostKeyPub";
	
	protected String algorithm = "RSA";
    protected int keySize;
    protected AlgorithmParameterSpec keySpec;

	protected String moduleConfigPath;
    
    
    public MgnlKeypairProvider(final String moduleConfigPath){
    	this.moduleConfigPath = moduleConfigPath;
    }
    
	
	@Override
	synchronized public Iterable<KeyPair> loadKeys() {
		KeyPair kp = doLoadKey();
		if (kp==null){			
			kp = generateKeyPair();
			doWriteKey(kp);
		}
		List<KeyPair> result = new ArrayList<KeyPair>();
		if (kp!=null)
			result.add(kp);
		return result;
	}


	
	protected KeyPair doLoadKey(){
		log.debug("Loading key...");
		String[] keys = MgnlContext.doInSystemContext(new Op<String[],RuntimeException>(){
			@Override
			public String[] exec() throws RuntimeException {
				Node configNode = SessionUtil.getNode("config", moduleConfigPath); // TODO do we need to close this session??
				if (configNode==null)
					return null;
				return new String[]{ PropertyUtil.getString(configNode, HOSTKEY_PRIV_PROPERTY_NAME, null), PropertyUtil.getString(configNode, HOSTKEY_PUB_PROPERTY_NAME, null) };
			}    		
    	});
		
		if (keys==null || keys.length!=2 || StringUtils.isEmpty(keys[0]) || StringUtils.isEmpty(keys[1]))
			return null;
		
		KeyPair kp = null;
		try {
			byte[] privKey = SecurityUtil.hexToByteArray(keys[0]);
			byte[] pubKey = SecurityUtil.hexToByteArray(keys[1]);
			PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privKey);
            KeyFactory kf = KeyFactory.getInstance(algorithm, "BC");
            PrivateKey pk = kf.generatePrivate(privateKeySpec);
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(pubKey);
            kf = KeyFactory.getInstance(algorithm, "BC");
            PublicKey pubk = kf.generatePublic(publicKeySpec);
            kp = new KeyPair(pubk, pk);
            log.debug("Keys loaded.");
		} catch (Exception e) {
			log.error("Problem reading key.",e);
		}
		return kp;
	}
	
	
	
	protected boolean doWriteKey(KeyPair kp){
		final String privateKey = SecurityUtil.byteArrayToHex(kp.getPrivate().getEncoded());
		final String publicKey = SecurityUtil.byteArrayToHex(kp.getPublic().getEncoded());
		return MgnlContext.doInSystemContext(new Op<Boolean,RuntimeException>(){
			@Override
			public Boolean exec() throws RuntimeException {
				Node configNode = SessionUtil.getNode("config", moduleConfigPath);
				if (configNode==null){
					log.warn("Module config node did not exist, can't write key...");
					return false;
				}
				//pemString = sb.toString();
				try {
					PropertyUtil.setProperty(configNode, HOSTKEY_PRIV_PROPERTY_NAME, privateKey); 
					PropertyUtil.setProperty(configNode, HOSTKEY_PUB_PROPERTY_NAME, publicKey);
					configNode.getSession().save(); // TODO also close the session??
					return true;
				} catch (RepositoryException e) {
					log.error("Can't write key...",e);
				}
				return false;
			}
		});
	}
	
	
	
    protected KeyPair generateKeyPair() {
        try {
            KeyPairGenerator generator = SecurityUtils.getKeyPairGenerator(algorithm);
            if (keySpec != null) {
                generator.initialize(keySpec);
            } else if (keySize != 0) {
                generator.initialize(keySize);
            }
            log.info("Generating host key...");
            KeyPair kp = generator.generateKeyPair();
            return kp;
        } catch (Exception e) {
            log.warn("Unable to generate keypair", e);
            return null;
        }
    }

	

}
