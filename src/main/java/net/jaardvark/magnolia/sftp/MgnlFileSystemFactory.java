package net.jaardvark.magnolia.sftp;

import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;

import org.apache.sshd.common.file.FileSystemFactory;
import org.apache.sshd.common.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MgnlFileSystemFactory implements FileSystemFactory {

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(MgnlFileSystemFactory.class);

	public static final URI uri = URI.create("mgnl:///");
	
	
	public MgnlFileSystemFactory(){
	}
	

	@Override
	public FileSystem createFileSystem(Session session) throws IOException {
		log.debug("Creating new SSH FileSystem for user: "+session.getUsername());
		FileSystem fs;
		// TODO use provider
		fs = FileSystems.newFileSystem(uri, null, this.getClass().getClassLoader());
		//session.addListener(view);
		return fs;
	}

}
