package net.jaardvark.magnolia.sftp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.auth.UserAuth;
import org.apache.sshd.server.auth.password.UserAuthPasswordFactory;
import org.apache.sshd.server.auth.pubkey.UserAuthPublicKeyFactory;
import org.apache.sshd.server.scp.ScpCommandFactory;
import org.apache.sshd.server.subsystem.sftp.SftpSubsystemFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Implements SFTP Server in magnolia
 * Provides access to JCR via SFTP
 * 
 * @author Richard Unger
 */
public class MgnlSftpServer {

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(MgnlSftpServer.class);
	
	protected SshServer sshd;
	
	
	public void startServer(int port, String moduleName) {
		 
        log.info("SFTP service starting...");
 
        sshd = SshServer.setUpDefaultServer();
        sshd.setPort(port);
        sshd.setKeyPairProvider(new MgnlKeypairProvider("/modules/" + moduleName + "/config"));
        sshd.setFileSystemFactory(new MgnlFileSystemFactory());
        sshd.setCommandFactory(new ScpCommandFactory());
        
        List<NamedFactory<Command>> namedFactoryList = new ArrayList<NamedFactory<Command>>();
        namedFactoryList.add(new SftpSubsystemFactory());
        sshd.setSubsystemFactories(namedFactoryList); 
        
        List<NamedFactory<UserAuth>> userAuthFactories = new ArrayList<NamedFactory<UserAuth>>();
        userAuthFactories.add(new UserAuthPublicKeyFactory());
        userAuthFactories.add(new UserAuthPasswordFactory());
        sshd.setUserAuthFactories(userAuthFactories);
        sshd.setPublickeyAuthenticator(new MgnlPublicKeyAuthenticator());
        sshd.setPasswordAuthenticator(new MgnlPasswordAuthenticator());

        try {
            sshd.start();
            log.info("SFTP service started.");
        } catch (Exception e) {
            log.error("Problem starting SFTP service: "+e.getMessage(), e);
        }
	
	}
	
	
	public void stopServer() {
		log.info("Stopping SFTP service (waiting for resources to be released)...");
		try {
			sshd.stop();
			log.info("SFTP service stopped.");
		} catch (IOException e) {
			log.error("SFTP service shutdown was interrupted.", e);
		}
	}
	
	
}
