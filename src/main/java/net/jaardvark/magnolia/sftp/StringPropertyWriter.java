package net.jaardvark.magnolia.sftp;

import info.magnolia.jcr.util.NodeUtil;

import java.io.IOException;
import java.io.Writer;

import javax.jcr.Node;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringPropertyWriter extends Writer {

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(StringPropertyWriter.class);
	
	protected Node node;
	protected String propertyName;
	protected long offset;
	
	protected StringBuffer buffer = new StringBuffer();

	public StringPropertyWriter(Node node, String propertyName, long offset) {
		this.node = node;
		this.propertyName = propertyName;
		this.offset = offset;
	}

	@Override
	public void close() throws IOException {
		// write to node
		try {
			node.setProperty(propertyName, buffer.toString());
			node.getSession().save();
		} catch (Exception e) {
			log.error("Problem setting property "+propertyName+" in node "+NodeUtil.getName(node), e);
		}
	}

	@Override
	public void flush() throws IOException {
		// ignore
	}

	@Override
	public void write(char[] chars, int offset, int length) throws IOException {
		buffer.append(chars,offset,length);
	}

}
