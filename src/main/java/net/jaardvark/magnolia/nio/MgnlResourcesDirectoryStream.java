package net.jaardvark.magnolia.nio;

import info.magnolia.resourceloader.Resource;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.apache.sshd.common.file.util.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Traverses resource folders
 */
public class MgnlResourcesDirectoryStream implements DirectoryStream<Path>, Iterator<Path> {

	public static final Logger log = LoggerFactory.getLogger(MgnlResourcesDirectoryStream.class);
	
	protected Resource folder;
	protected Iterator<Resource> iterator;
	protected Path next = null;
	protected boolean hasNextCalled;
	protected MgnlFileSystem fs;
	protected java.nio.file.DirectoryStream.Filter<? super Path> filter;

	public MgnlResourcesDirectoryStream(Resource folder, Filter<? super Path> filter, MgnlFileSystem fs) {
		this.folder = folder;
		this.filter = filter;
		this.fs = fs;
		// get children
		// TODO check read permission on child? lets assume for the moment the resource abstraction handles it...
		this.iterator = folder.listChildren().iterator();
	}

	
	@Override
	public void close() throws IOException {
		// nix
	}

	@Override
	public Iterator<Path> iterator() {
		// filter
		return this;
	}
	

	@Override
	public boolean hasNext() {
		hasNextCalled = true;
		boolean accepted = false;
		do {
			if (!iterator.hasNext()){
				next = null;
				return false;
			}
			Resource r = iterator.next();
			if (r==null){
				next = null;
				return false;
			}
			next = new MgnlPath(fs,"/", new ImmutableList<>(StringUtils.split(r.getPath(),'/')));
			try {
				accepted = filter.accept(next);
			} catch (IOException e) {
				accepted = false;
				next = null;
				log.warn("IOException in filter: "+e.getMessage(), e);
			}
		}
		while (!accepted);
		return true;
	}

	@Override
	public Path next() {
		if (!hasNextCalled)
			hasNext();
		hasNextCalled = false;
		return next;
	}

	

}
