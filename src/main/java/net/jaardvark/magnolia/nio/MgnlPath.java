package net.jaardvark.magnolia.nio;

import java.io.IOException;
import java.nio.file.LinkOption;
import java.nio.file.Path;

import org.apache.sshd.common.file.util.BasePath;
import org.apache.sshd.common.file.util.ImmutableList;

/**
 * Simple extension to BasePath.
 * 
 * MgnlPaths have '/' as root, and don't have SymLinks or special elements like
 * ~, . or ..
 * 
 * @author Richard Unger
 */
public class MgnlPath extends BasePath<MgnlPath, MgnlFileSystem> {

	public MgnlPath(MgnlFileSystem mgnlFileSystem, String root, ImmutableList<String> names) {
		super(mgnlFileSystem, root, names);
	}

	@Override
	public Path toRealPath(LinkOption... options) throws IOException {
		return this.toAbsolutePath();
	}

}
