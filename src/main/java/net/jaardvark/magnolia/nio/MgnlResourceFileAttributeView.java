package net.jaardvark.magnolia.nio;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.JcrConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Resources;

import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.resourceloader.AbstractResource;
import info.magnolia.resourceloader.Resource;
import info.magnolia.resourceloader.classpath.ClasspathResource;
import info.magnolia.resourceloader.classpath.service.impl.URLConnectionUtil;
import info.magnolia.resourceloader.file.FileSystemResource;
import info.magnolia.resourceloader.file.FileSystemResourceVisibilityHack;
import info.magnolia.resourceloader.jcr.JcrResource;
import info.magnolia.resourceloader.jcr.JcrResourceOrigin;
import info.magnolia.resourceloader.jcr.JcrResourceVisibilityHack;

public class MgnlResourceFileAttributeView implements BasicFileAttributeView, BasicFileAttributes {

	public final static Logger log = LoggerFactory.getLogger(MgnlResourceFileAttributeView.class);
	
	protected Resource r;
	
	public MgnlResourceFileAttributeView(Resource r) {
		this.r = r;
	}

	@Override
	public String name() {
		return "basic";
	}

	@Override
	public BasicFileAttributes readAttributes() throws IOException {
		return this;
	}

	@Override
	public void setTimes(FileTime lastModifiedTime, FileTime lastAccessTime, FileTime createTime) throws IOException {
		log.warn("Attempt to setTimes() but this functionality is not implemented!");
	}

	@Override
	public FileTime lastModifiedTime() {
		if ("/".equals(r.getPath()))
			return FileTime.fromMillis(0);
		try {
			return FileTime.fromMillis(r.getLastModified());
		}
		catch (Exception ex){
			// be silent, magnolia is logging this anyway... log.debug("Could not get lastModifiedTime for "+r.getPath(), ex);
		}
		return FileTime.fromMillis(0);
	}

	@Override
	public FileTime lastAccessTime() {
		// TODO this is not the correct value, lets see what we can do to improve this...
		return lastModifiedTime();
	}

	@Override
	public FileTime creationTime() {
		// TODO this is not the correct value, lets see what we can do to improve this...
		return lastModifiedTime();
	}

	@Override
	public boolean isRegularFile() {
		return r.isFile();
	}

	@Override
	public boolean isDirectory() {
		return r.isDirectory();
	}

	@Override
	public boolean isSymbolicLink() {
		return false;
	}

	@Override
	public boolean isOther() {
		return false;
	}

	@Override
	public long size() {
		if (r instanceof JcrResource){
			// why, oh why does magnolia persist in making everything private or protected?
			// it renders the APIs next to useless. What's the point in having a ResourceOrigin API and JcrResource objects if
			// you then can't use them to get at the content, and have to re-code all the access methods yourself?
			Node node = JcrResourceVisibilityHack.getNode(r);
			try {
				if (node.isNodeType(NodeTypes.Folder.NAME) || node.getDepth() == 0)
					return 0;
				if (node.hasProperty(JcrResourceOrigin.TEXT_PROPERTY))
					return node.getProperty(JcrResourceOrigin.TEXT_PROPERTY).getLength();
				if (node.hasNode(JcrResourceOrigin.BINARY_NODE_NAME)){
					Node binary = node.getNode(JcrResourceOrigin.BINARY_NODE_NAME);
					if (!binary.hasProperty(JcrConstants.JCR_DATA))
						return 0;
				    return binary.getProperty(JcrConstants.JCR_DATA).getLength();
				}
			} catch (RepositoryException e) {
				log.error("Problem getting size of binary node.");
			}			
		}
		else if (r instanceof ClasspathResource){
			@SuppressWarnings("rawtypes")
			String absPath = ((ClasspathResource)r).getClasspathEntry().getAbsolutePath();
			URL u = Resources.getResource(absPath);
			URLConnection uc = null;
			try {
				uc = u.openConnection();
				return uc.getContentLengthLong();
			} catch (IOException e) {
				log.error("Could not get size of classpath resource.",e);
			}
			finally {
				URLConnectionUtil.closeURLConnection(uc);
			}
		}
		else if (r instanceof FileSystemResource){
			// incredibly annoyingly the .getRealPath() method is not visible, as if the real filename is big secret.
			// obviously implementations using the API would like to access the underlying storage in some situations.
			Path p = FileSystemResourceVisibilityHack.getRealPath(r);		
			File f = p.toAbsolutePath().toFile();
			return f.length();
		}
		else if (r instanceof AbstractResource){
			// we could read the stream to get the size...
			InputStream is = null;
			try {
				is = r.openStream();
				return is.skip(Long.MAX_VALUE);
			} catch (IOException e) {
				log.error("Problem determining size of stream.",e);
			}
			finally {
				IOUtils.closeQuietly(is);
			}
		}
		log.warn("Don't know how to handle resources of type: "+r.getClass().getName());
		return 0;
	}

	@Override
	public Object fileKey() {
		return r.getOrigin().getName()+":"+r.getPath();
	}


}
