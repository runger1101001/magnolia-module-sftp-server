package net.jaardvark.magnolia.nio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.GatheringByteChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.ScatteringByteChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.channels.WritableByteChannel;

import javax.jcr.AccessDeniedException;
import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFactory;

import org.apache.commons.io.IOUtils;

import info.magnolia.resourceloader.Resource;
import info.magnolia.resourceloader.jcr.JcrResourceOrigin;
import info.magnolia.resourceloader.jcr.JcrResourceVisibilityHack;

public class SeekableByteChannelWrapper extends FileChannel implements SeekableByteChannel, GatheringByteChannel, ScatteringByteChannel {

	protected final MgnlFileSystemProvider mgnlFileSystemProvider;
	
	protected FileChannel wrappedFileChannel;

	protected File fsFile;

	protected Resource file;

	protected boolean write;

	public SeekableByteChannelWrapper(MgnlFileSystemProvider mgnlFileSystemProvider, FileChannel wrappedFileChannel, File fsFile, Resource resource, boolean write) {
		this.mgnlFileSystemProvider = mgnlFileSystemProvider;
		this.wrappedFileChannel = wrappedFileChannel;
		this.fsFile = fsFile;
		this.file = resource;
		this.write = write;
	}


	@Override
	public void implCloseChannel() throws IOException {
		wrappedFileChannel.close();
		if (write){
			// now copy the data back...
			Node n = JcrResourceVisibilityHack.getNode(file);
			try {
				ValueFactory vf = n.getSession().getValueFactory();
				if (JcrResourceVisibilityHack.isBinaryResource(n)){
					Binary binary = vf.createBinary(new FileInputStream(fsFile));
					n.setProperty("binary/jcr:data", binary);
					n.getSession().save();
				}
				else if (JcrResourceVisibilityHack.isTextResource(n)){
					n.setProperty(JcrResourceOrigin.TEXT_PROPERTY, IOUtils.toString(new FileReader(fsFile)));
					n.getSession().save();
				}
				else
					throw new IOException("Resource file "+file.getPath()+" was neither text nor binary type!");
			} catch (AccessDeniedException e) {
				throw new IOException("Permission denied.",e);
			} catch (RepositoryException e) {
				throw new IOException("Problem writing content back to JCR.",e);
			}
			// delete the temporary file
			fsFile.delete();
			// and fix up the node...
			mgnlFileSystemProvider.updateResourceType(n);
		}
	}

	public int read(ByteBuffer dst) throws IOException {
		return wrappedFileChannel.read(dst);
	}

	public int write(ByteBuffer src) throws IOException {
		return wrappedFileChannel.write(src);
	}

	public long position() throws IOException {
		return wrappedFileChannel.position();
	}

	public SeekableByteChannelWrapper position(long newPosition) throws IOException {
		wrappedFileChannel.position(newPosition);
		return this;
	}

	public long size() throws IOException {
		return wrappedFileChannel.size();
	}

	public SeekableByteChannelWrapper truncate(long size) throws IOException {
		wrappedFileChannel.truncate(size);
		return this;
	}

	public long read(ByteBuffer[] dsts, int offset, int length) throws IOException {
		return wrappedFileChannel.read(dsts, offset, length);
	}

	public long write(ByteBuffer[] srcs, int offset, int length) throws IOException {
		return wrappedFileChannel.write(srcs, offset, length);
	}

	public void force(boolean metaData) throws IOException {
		wrappedFileChannel.force(metaData);
	}

	public long transferTo(long position, long count, WritableByteChannel target) throws IOException {
		return wrappedFileChannel.transferTo(position, count, target);
	}

	public long transferFrom(ReadableByteChannel src, long position, long count) throws IOException {
		return wrappedFileChannel.transferFrom(src, position, count);
	}

	public int read(ByteBuffer dst, long position) throws IOException {
		return wrappedFileChannel.read(dst, position);
	}

	public int write(ByteBuffer src, long position) throws IOException {
		return wrappedFileChannel.write(src, position);
	}

	public MappedByteBuffer map(MapMode mode, long position, long size) throws IOException {
		return wrappedFileChannel.map(mode, position, size);
	}

	public FileLock lock(long position, long size, boolean shared) throws IOException {
		return wrappedFileChannel.lock(position, size, shared);
	}

	public FileLock tryLock(long position, long size, boolean shared) throws IOException {
		return wrappedFileChannel.tryLock(position, size, shared);
	}


}