package net.jaardvark.magnolia.nio;

import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.Collections;
import java.util.Set;

import org.apache.sshd.common.file.util.BaseFileSystem;
import org.apache.sshd.common.file.util.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

/**
 * FileSystem for magnolia resources.
 * 
 * Paths are '/' separated.
 * 
 * TODO get the magnolia session into this object... or do we work via MgnlContext?
 * 
 * @author Richard Unger
 */
public class MgnlFileSystem extends BaseFileSystem<MgnlPath> {
		
	protected FileStore mgnlFileStore = new MgnlFileStore();

	public static final Logger log = LoggerFactory.getLogger(MgnlFileSystem.class);
	
	protected boolean closed = false;
	
	public final static Set<String> SUPPORTED_ATTRIBUTE_VIEWS = Sets.newHashSet("posix", "basic" );
	
	MgnlFileSystem(MgnlFileSystemProvider provider){
		super(provider);
		log.debug("MgnlFileSystem instantiated.");
	}	


	@Override
	public void close() throws IOException {
		log.debug("MgnlFileSystem closed.");
		closed = true;
	}

	@Override
	public boolean isOpen() {
		return closed;
	}

	@Override
	public Iterable<FileStore> getFileStores() {
		return Collections.<FileStore>singleton( mgnlFileStore );
	}

	@Override
	public Set<String> supportedFileAttributeViews() {
		return SUPPORTED_ATTRIBUTE_VIEWS;
	}


	@Override
	public UserPrincipalLookupService getUserPrincipalLookupService() {
		throw new UnsupportedOperationException("UserPrincipalLookupService currently not implemented.");
	}
	
	@Override
	protected MgnlPath create(String root, ImmutableList<String> names) {
		return new MgnlPath(this, root, names);
	}

}
