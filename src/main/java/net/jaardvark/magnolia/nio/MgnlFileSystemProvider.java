package net.jaardvark.magnolia.nio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.AccessMode;
import java.nio.file.CopyOption;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.FileStore;
import java.nio.file.FileSystem;
import java.nio.file.LinkOption;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.FileAttributeView;
import java.nio.file.spi.FileSystemProvider;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.objectfactory.Components;
import info.magnolia.resourceloader.Resource;
import info.magnolia.resourceloader.ResourceOrigin;
import info.magnolia.resourceloader.jcr.JcrResource;
import info.magnolia.resourceloader.jcr.JcrResourceVisibilityHack;
import info.magnolia.resourceloader.layered.LayeredResource;


/**
 * Provides FileSystemProvider implementation for magnolia resources.
 * 
 * Magnolia resources are mapped from their various storage locations into SFTP.
 * JCR ( TODO classpath and file-based?) resources are writeable if the user has 
 * sufficient priveleges.
 * Classpath and Filesystem resources are currently not handled.
 * 
 * When creating resources, nodes are created in JCR, based on the file extension. 
 * Supported extensions that create text resources are:
 *    css, ftl, xslt, xml, txt, js, yaml.
 * Everything else will create a binary resource. The required JCR properties are set
 * automatically. Be careful overwriting custom resource types via SFTP - existing JCR 
 * properties will likely be overwritten.
 * 
 * @author Richard Unger
 */
public class MgnlFileSystemProvider extends FileSystemProvider {

	public final static String MGNL_SCHEME = "mgnl";
	
	protected MgnlFileSystem resourceFS;
	
	final static public Logger log = LoggerFactory.getLogger(MgnlFileSystemProvider.class);

	@SuppressWarnings("rawtypes")
	protected ResourceOrigin resourceOrigins;
	
	public MgnlFileSystemProvider(){
	}
	
	
	@Override
	public String getScheme() {
		return MGNL_SCHEME;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public FileSystem newFileSystem(URI uri, Map<String, ?> env) throws IOException {
		if (!MGNL_SCHEME.equalsIgnoreCase(uri.getScheme()))
			return null;
		if (resourceFS!=null) // return existing instance if available
			return resourceFS;
		ResourceOrigin resourceOrigin = Components.getComponent(ResourceOrigin.class);
		resourceOrigins = ((LayeredResource)resourceOrigin.getByPath("/")).getFirst().getOrigin(); // hack to get at JCR resource origin      
		resourceFS = new MgnlFileSystem(this);
		log.debug("MgnlFileSystemProvider initialized.");
		return resourceFS;
	}

	@Override
	public FileSystem getFileSystem(URI uri) {
		if (MGNL_SCHEME.equalsIgnoreCase(uri.getScheme())){
			if (resourceFS==null)
				log.debug("MgnlFileSystemProvider was not initialized!");
			return resourceFS;
		}
		return null;
	}

	@Override
	public Path getPath(URI uri) {
		String path = uri.getPath();
		return resourceFS.getPath(path);
	}
	
	
	
	@Override
	public SeekableByteChannel newByteChannel(Path path, Set<? extends OpenOption> options, FileAttribute<?>... attrs) throws IOException {
		return newFileChannel(path, options, attrs);
	}
	


	@Override
	public FileChannel newFileChannel(Path path, Set<? extends OpenOption> options, FileAttribute<?>... attrs) throws IOException {
		// check if it exists
		// if existing, check read/write permission
		Resource file = null;
		if (resourceOrigins.hasPath(path.toString())){
			checkAccess(path, AccessMode.WRITE); // TODO add_node should be sufficient, update this later
			file = resourceOrigins.getByPath(path.toString());
			if (!file.isFile())
				throw new IOException("Attempt to write to non-file: "+path.toAbsolutePath());
		}
		else {
			// if new, check addNode permission on parent
			Path parent = path.getParent();
			if (parent==null)
				throw new IOException("Parent path could not be determined.");
			if (!resourceOrigins.hasPath(parent.toString()))
				throw new IOException("Parent does not exist.");
			Resource folder = resourceOrigins.getByPath(parent.toString());
			if (!folder.isDirectory())
				throw new IOException(parent.toAbsolutePath()+" is not a directory.");
			checkAccess(parent, AccessMode.WRITE); // TODO add_node should be sufficient, update this later
		}
		
			
		// we have to return a temporary file here, since JCR won't allow a channel
		// so return an object which writes back to JCR once the channel gets closed...		
		File fsFile = File.createTempFile("mgnl-sftp", null);
		if (file!=null){
			log.debug("Copying existing JCR data from "+file.getPath()+" to temporary file "+fsFile.getAbsolutePath());
			IOUtils.copy(file.openStream(), new FileOutputStream(fsFile));
		}
		else {
			// TODO create a new node
		}
		FileChannel tempChannel = FileChannel.open(fsFile.toPath(), options);
		return new SeekableByteChannelWrapper(this, tempChannel, fsFile, file, options.contains(StandardOpenOption.WRITE));
	}

	
	
	
	
	

	@Override
	public DirectoryStream<Path> newDirectoryStream(Path dir, Filter<? super Path> filter) throws IOException {
		// check read permission on parent
		checkAccess(dir, AccessMode.READ);
		Resource folder = resourceOrigins.getByPath(dir.toString());
		if (!folder.isDirectory())
			throw new IOException(dir.toAbsolutePath()+" is not a directory.");		
		// return DirectoryStream
		return new MgnlResourcesDirectoryStream(folder, filter, resourceFS);
	}

	@Override
	public void createDirectory(Path dir, FileAttribute<?>... attrs) throws IOException {
		// check if it already exists...
		// check addNode permission on parent
		// create it in JCR
		if (resourceOrigins.hasPath(dir.toString()))
			throw new IOException("Directory or file "+dir.toString()+" exists.");
		Path parent = dir.getParent();
		if (parent==null)
			throw new IOException("Parent path could not be determined.");
		if (!resourceOrigins.hasPath(parent.toString()))
			throw new IOException("Parent does not exist.");
		Resource folder = resourceOrigins.getByPath(parent.toString());
		if (!folder.isDirectory())
			throw new IOException(parent.toAbsolutePath()+" is not a directory.");
		checkAccess(parent, AccessMode.WRITE); // TODO add_node should be sufficient, update this later
		
		Node n = JcrResourceVisibilityHack.getNode(folder);
		try {
			n.addNode(dir.getFileName().toString(),NodeTypes.Folder.NAME);
		} catch (ItemExistsException e) {
			throw new IOException("Directory or file "+dir.toString()+" exists.",e);
		} catch (RepositoryException e) {
			throw new IOException("Problem creating directory in JCR.",e);
		}
	}

	@Override
	public void delete(Path path) throws IOException {
		// get node 
		Resource r = resourceOrigins.getByPath(path.toString());
		if (r!=null && r instanceof JcrResource){
			// check delete permission
			checkAccess(path, AccessMode.WRITE); // TODO do we need write on parent to delete a node? check JCR spec
			// delete node
			Node n = JcrResourceVisibilityHack.getNode(r);
			try {
				Node parent = n.getParent();
				n.remove();
				parent.getSession().save();
			} catch (RepositoryException e) {
				log.error("Problem removing node "+path,e);
			}
		}
	}

	@Override
	public void copy(Path source, Path target, CopyOption... options) throws IOException {
		String sFile = source.getFileName().toString();
		String tFile = target.getFileName().toString();
		// check delete permission on source
		checkAccess(source.getParent(), AccessMode.READ);
		// check addNode permission on destination parent
		checkAccess(target.getParent(), AccessMode.WRITE); // TODO for now, use write instead of addNode
		// check target does not exist
		Resource t = resourceOrigins.getByPath(target.toString());
		if (t!=null)
			throw new IOException("Copy target "+target.toString()+" exists.");		
		// copy (to JCR)
		try {
			Node sN = JcrResourceVisibilityHack.getNode(resourceOrigins.getByPath(source.toString()));
			Session sess = sN.getSession();
			sess.move(source.toString(), target.toString());
			Node tN = sess.getNode(target.toString());
			if (!sFile.equals(tFile)){
				// filename changed, so update binary/text type if needed
				updateResourceType(tN);
			}
			sess.save();
		} catch (RepositoryException e) {
			log.error("Repository exception moving "+source+" to "+target, e);
			throw new IOException("Repository exception moving "+source+" to "+target, e);
		}
	}

	@Override
	public void move(Path source, Path target, CopyOption... options) throws IOException {
		String sFile = source.getFileName().toString();
		String dFile = target.getFileName().toString();
		// check delete permission on source
		checkAccess(source.getParent(), AccessMode.WRITE);
		// check addNode permission on destination parent
		checkAccess(target.getParent(), AccessMode.WRITE); // TODO for now, use write instead of addNode
		// check target does not exist
		Resource t = resourceOrigins.getByPath(target.toString());
		if (t!=null)
			throw new IOException("Move target "+target.toString()+" exists.");		
		// move/rename (in JCR)
		try {
			Node sN = JcrResourceVisibilityHack.getNode(resourceOrigins.getByPath(source.toString()));
			Session sess = sN.getSession();
			sess.move(source.toString(), target.toString());
			Node tN = sess.getNode(target.toString());
			if (!sFile.equals(dFile)){
				// filename changed, so update binary/text type if needed
				updateResourceType(tN);
			}
			sess.save();
		} catch (RepositoryException e) {
			log.error("Repository exception moving "+source+" to "+target, e);
			throw new IOException("Repository exception moving "+source+" to "+target, e);
		}
		
	}

	/**
	 * Update the given node to correctly store the resource based on its
	 * extension (binary or text resource).
	 * @param t
	 */
	protected void updateResourceType(Node t) {
		// TODO Auto-generated method stub
		
	}

	
	

	@Override
	public boolean isSameFile(Path path, Path path2) throws IOException {
		return path.compareTo(path2)==0;
	}

	@Override
	public boolean isHidden(Path path) throws IOException {
		return false;
	}

	@Override
	public FileStore getFileStore(Path path) throws IOException {
		return resourceFS.getFileStores().iterator().next();
	}

	@Override
	public void checkAccess(Path path, AccessMode... modes) throws IOException {
		// TODO Auto-generated method stub
		
		// check if path exists
		// if so, check access modes, if any
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public <V extends FileAttributeView> V getFileAttributeView(Path path, Class<V> type, LinkOption... options) {
		//if (!resourceFS.supportedFileAttributeViews().contains(type.getN))
		//	return null;
		
		// check read permissions
		try {
			checkAccess(path, AccessMode.READ);
		} catch (IOException e) {
			log.error("checkAccess exception",e);
			return null;
		}
		// return new BasicFileAttributeView
		Resource r = resourceOrigins.getByPath(path.toString());
		return (V)new MgnlResourcePosixFileAttributeView(r);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <A extends BasicFileAttributes> A readAttributes(Path path, Class<A> type, LinkOption... options) throws IOException {
		// check read permissions
		try {
			checkAccess(path, AccessMode.READ);
		} catch (IOException e) {
			log.error("checkAccess exception",e);
			return null; // TODO throw exception?
		}
		// return new BasicFileAttributes
		MgnlResourcePosixFileAttributeView av = getFileAttributeView(path, MgnlResourcePosixFileAttributeView.class, options);
		return (A) av;
	}

	@Override
	public Map<String, Object> readAttributes(Path path, String attributes, LinkOption... options) throws IOException {
		try {
			checkAccess(path, AccessMode.READ);
		} catch (IOException e) {
			log.error("checkAccess exception",e);
			return null; // TODO throw exception?
		}
		MgnlResourcePosixFileAttributeView av = getFileAttributeView(path, MgnlResourcePosixFileAttributeView.class, options);
		Map<String, Object> result = new HashMap<String,Object>();
		String prefix = null;
		if (attributes.contains(":")){
			prefix = StringUtils.substringBefore(attributes, ":");
			if (!("basic".equals(prefix)||"posix".equals(prefix))){
				log.warn("Can only handle 'basic' or 'posix' file attributes...");
				return result;
			}
			attributes = StringUtils.substringAfter(attributes, ":");
		}
		String[] attNames = attributes.split(",");
		for (String attName : attNames){
			switch (attName){
			case "size":
				result.put("size", av.size());
				continue;
			case "lastAccessTime":
				result.put("lastAccessTime", av.lastAccessTime());
				continue;
			case "lastModifiedTime":
				result.put("lastModifiedTime", av.lastModifiedTime());
				continue;
			case "creationTime":
				result.put("creationTime", av.creationTime());
				continue;
			case "isRegularFile":
				result.put("isRegularFile", av.isRegularFile());
				continue;
			case "isDirectory":
				result.put("isDirectory", av.isDirectory());
				continue;
			case "isSymbolicLink":
				result.put("isSymbolicLink", av.isSymbolicLink());
				continue;
			case "isOther":
				result.put("isOther", av.isOther());
				continue;
			case "fileKey":
				result.put("fileKey", av.fileKey());
				continue;
			case "owner":
				result.put("isOther", av.owner());
				continue;
			case "group":
				result.put("isOther", av.group());
				continue;
			case "permissions":
				result.put("isOther", av.permissions());
				continue;
			case "*":
				if ("posix".equals(prefix)){
					result.put("owner", av.owner());
					result.put("group", av.group());
					result.put("permissions", av.permissions());					
				}
				else {
					result.put("size", av.size());
					result.put("lastAccessTime", av.lastAccessTime());
					result.put("lastModifiedTime", av.lastModifiedTime());
					result.put("creationTime", av.creationTime());
					result.put("isRegularFile", av.isRegularFile());
					result.put("isDirectory", av.isDirectory());
					result.put("isSymbolicLink", av.isSymbolicLink());
					result.put("isOther", av.isOther());
					result.put("fileKey", av.fileKey());
				}
				continue;
			default:
				log.warn("Don't know the file attribute '"+attName+"'...");
			}
		}
		return result;
	}

	@Override
	public void setAttribute(Path path, String attribute, Object value, LinkOption... options) throws IOException {
		log.error("Can't setAttribute on "+path+". Not implemented");
	}

}
