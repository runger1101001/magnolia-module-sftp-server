package net.jaardvark.magnolia.nio;

import java.io.IOException;
import java.nio.file.attribute.GroupPrincipal;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.UserPrincipal;
import java.util.HashSet;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sshd.server.subsystem.sftp.DefaultGroupPrincipal;
import org.apache.sshd.server.subsystem.sftp.DefaultUserPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.resourceloader.Resource;
import info.magnolia.resourceloader.jcr.JcrResourceVisibilityHack;

public class MgnlResourcePosixFileAttributeView extends MgnlResourceFileAttributeView implements PosixFileAttributeView, PosixFileAttributes {

	public final static Logger log = LoggerFactory.getLogger(MgnlResourcePosixFileAttributeView.class);

	public final static DefaultGroupPrincipal GROUP = new DefaultGroupPrincipal("jcr");
	
	public MgnlResourcePosixFileAttributeView(Resource r) {
		super(r);
	}

	@Override
	public String name() {
		return "posix";
	}


	@Override
	public UserPrincipal owner() {
		Node n = JcrResourceVisibilityHack.getNode(r);
		String owner = "superuser";
		try {
			owner = NodeTypes.Created.getCreatedBy(n);
		} catch (RepositoryException e) {
			log.warn("Can't get creator for node "+NodeUtil.getNodePathIfPossible(n));
		}
		if (StringUtils.isEmpty(owner))
			owner = "superuser";
		return new DefaultUserPrincipal(owner);
	}

	@Override
	public GroupPrincipal group() {
		return GROUP; // return fixed notion of group, there is no such direct concept in jcr
	}

	@Override
	public Set<PosixFilePermission> permissions() {
		// we don't normally see nodes we don't have permission to read in JCR
		// TODO check this assumption carefully, since resources code seems to use system session (ugh!)
		HashSet<PosixFilePermission> result = new HashSet<PosixFilePermission>();
		result.add(PosixFilePermission.OWNER_READ);
		result.add(PosixFilePermission.OTHERS_READ);
		if (isDirectory()){
			result.add(PosixFilePermission.OWNER_EXECUTE);
			result.add(PosixFilePermission.OTHERS_EXECUTE);			
		}
		Node n = JcrResourceVisibilityHack.getNode(r);
		try {
			if (n.getSession().hasPermission(n.getPath(), "add_node")){
				result.add(PosixFilePermission.OWNER_WRITE);
				result.add(PosixFilePermission.OTHERS_WRITE);			
			}
		} catch (RepositoryException e) {
			log.warn("Can't check write permission for node "+NodeUtil.getNodePathIfPossible(n));
		}			
		return result;
	}
	
	@Override
	public void setOwner(UserPrincipal owner) throws IOException {
		throw new UnsupportedOperationException("Setting owner not supported.");
	}

	@Override
	public PosixFileAttributes readAttributes() throws IOException {
		return this;
	}

	@Override
	public void setPermissions(Set<PosixFilePermission> perms) throws IOException {
		throw new UnsupportedOperationException("Setting permissions not supported.");
	}

	@Override
	public void setGroup(GroupPrincipal group) throws IOException {
		throw new UnsupportedOperationException("Setting group not supported.");
	}

	@Override
	public UserPrincipal getOwner() throws IOException {
		return owner();
	}

}
