package net.jaardvark.magnolia.nio;

import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.FileAttributeView;
import java.nio.file.attribute.FileStoreAttributeView;

/**
 * Simple FileStore implementation representing storage in magnolia's
 * resources.
 * 
 * Total, useable and unallocated space are always reported as 1GB.
 * 
 * BasicFileAttributes are supported, no others.
 * The MgnlFileStore itself has no readable or writeable attributes at this point.
= * 
 * @author Richard Unger
 */
public class MgnlFileStore extends FileStore {
	
	public final static long DEFAULT_SPACE = 1024 * 1024 * 1024; // 1 GB

	@Override
	public String name() {
		return "MagnoliaFS";
	}

	@Override
	public String type() {
		return "MagnoliaFS-resources";
	}

	@Override
	public boolean isReadOnly() {
		return false;
	}

	@Override
	public long getTotalSpace() throws IOException {
		return DEFAULT_SPACE;
	}

	@Override
	public long getUsableSpace() throws IOException {
		return DEFAULT_SPACE;
	}

	@Override
	public long getUnallocatedSpace() throws IOException {
		return DEFAULT_SPACE;
	}

	@Override
	public boolean supportsFileAttributeView(Class<? extends FileAttributeView> type) {
		if (type.equals(BasicFileAttributeView.class))
			return true;
		return false;
	}

	@Override
	public boolean supportsFileAttributeView(String name) {
		return ("basic".equals(name)||"posix".equals(name));
	}

	@Override
	public <V extends FileStoreAttributeView> V getFileStoreAttributeView(Class<V> type) {
		return null;
	}

	@Override
	public Object getAttribute(String attribute) throws IOException {
		return null;
	}

}
