package net.jaardvark.magnolia.helper;

import info.magnolia.cms.security.Permission;

import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;

public class PermissionConverter {
    /**
     * Return String-representation of permissions convert from provided long-permission (old).
     * Note: copeied from Magnolia's PermissionUtil class where they made this package-private for some reason
     */
    public static String convertPermissions(long oldPermissions) {
        StringBuilder permissions = new StringBuilder();
        if ((oldPermissions & Permission.ALL) == Permission.ALL) {
            permissions.append(Session.ACTION_ADD_NODE).append(",").append(Session.ACTION_READ).append(",").append(Session.ACTION_REMOVE + ",").append(Session.ACTION_SET_PROPERTY);
            // skip the rest to be sure we don't introduce duplicates.
        } else {
            if ((oldPermissions & Permission.WRITE) == Permission.WRITE) {
                if (permissions.length() > 0) {
                    permissions.append(",");
                }
                permissions.append(Session.ACTION_ADD_NODE);
            }
            if ((oldPermissions & Permission.READ) == Permission.READ) {
                if (permissions.length() > 0) {
                    permissions.append(",");
                }
                permissions.append(Session.ACTION_READ);
            }
            if ((oldPermissions & Permission.REMOVE) == Permission.REMOVE) {
                if (permissions.length() > 0) {
                    permissions.append(",");
                }
                permissions.append(Session.ACTION_REMOVE);
            }
            if ((oldPermissions & Permission.SET) == Permission.SET) {
                if (permissions.length() > 0) {
                    permissions.append(",");
                }
                permissions.append(Session.ACTION_SET_PROPERTY);
            }
        }
        final String result = permissions.toString();
        if (StringUtils.isEmpty(result)) {
            throw new IllegalArgumentException("Unknown permissions: " + oldPermissions);
        }
        return result;
    }

}
